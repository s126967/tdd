package workshop.tdd;

import java.util.Arrays;

public class Matrix {

    private final int rows;
    private final int cols;
    private final int[][] matrix;

    public Matrix(int[][] array) {
        this.rows = array.length;
        this.cols = array[0].length;
        this.matrix = copyArray(array);
    }

    public int getRows() {
        return this.rows;
    }

    public int getCols() {
        return this.cols;
    }

    public int getValue(int row, int col) {
        return this.matrix[row][col];
    }


    private int[][] copyArray(int[][] array) {
        int[][] temp = new int[array.length][];
        for (int i = 0; i < array.length; i++) {
            temp[i] = Arrays.copyOf(array[i], array[i].length);
        }
        return temp;
    }


}
